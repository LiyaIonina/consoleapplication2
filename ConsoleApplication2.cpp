#include <iostream>

using namespace std;

void FindOddNumber(int limit, bool isOdd) {
    int start = isOdd ? 1 : 2; 
    for (int i = start; i <= limit; i += 2) {
        cout << i << " ";
    }
    cout << endl;
}
   
int main() {
    FindOddNumber(30, true);
    FindOddNumber(15, false);

    return 0;
}






